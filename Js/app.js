//clave para acceder a la api
const credencial = {
    method: 'GET',
    headers: {
		'X-RapidAPI-Key': '94cd14c6c8mshb9fb9aeeb6fd5bdp1dabdajsn65456f29226c',
		'X-RapidAPI-Host': 'spotify23.p.rapidapi.com'
	}
};

//Carga los datos de la cancion solicitada en la pagina busqueda, si no existe la cancion, carga un mensaje de error ademas de algunas canciones recomendadas
async function cargarPagina2() {
    let url = new URLSearchParams(location.search);
    let tipo = Number(url.get("tipo"));

    let cancion = (url.get("busqueda"));
    let dato = document.getElementById('cancion');
    let contentHTML = '';

    cancion = await buscarCancion(cancion, 1);
    console.log(cancion)
    if (cancion.tracks.items == 0) {
        //Si no encuentra ninguna cancion inserta HTML de error
        contentHTML = `  <div class="col-12 text-center">
        <h2 style="color:white" >Lo sentimos mucho :(</h2>
        <img src="../images/caraTriste.png" alt="">
        <p style="color:white">Esa Canción no se encuentra en nuestra base de datos, intenta con otra.</p>
    </div>
    <div style="background-color:#5F5F5F;" container-fluid">
        <h2 style="font-family: 'Secular One', sans-serif;" class="text-center text-white p-5">PODRIAS INTENTAR CON ESTAS
        </h2>
        <div  id="" class="row p-2">
        `;

        //busca canciones aleatorias para insertarlas
        cancion = await buscarCancionRandom(8);
        for (v of cancion.tracks.items) {
            let nameC = v.data.name;
            if (nameC.length > 40) {
                nameC = nameC.slice(0, 40);
            }
            let imgCancion = "../images/NN.jpg";
            if (v.data.albumOfTrack.coverArt.sources.length != 0) {
                imgCancion = v.data.albumOfTrack.coverArt.sources[0].url;
            }
            
            let nameA = v.data.artists.items[0].profile.name;
            contentHTML += `<div class="col-12 col-md-4 col-lg-3">
            <div class="border border-1 card text-white bg-dark mb-3" style="width: 18rem; background: none;">
                <img width="500" height="300" style="border-radius: 80%" class="p-5 card-img-top" src="${imgCancion}" alt="Card image cap">
                <div class="card-body">
                    <a href="Busqueda.html?busqueda=${nameC}"> <p style="color: white;" class="text-center  card-text">${nameC}</p></a>
                </div>
            </div>
        </div>`;
        }
        contentHTML += `
        </div>
    </div>
        `;
    } else {
        //si encuentra la cancion procede a insertar el codigo HTML para mostrarla
        cancion = cancion.tracks.items[0].data;
        let artista = await buscarArtista(cancion.artists.items[0].profile.name);
        console.log(artista)
        artista = artista.artists.items[0].data;

        //datos de la cancion
        let urlC = cancion.uri;
        let nameC = cancion.name;
        let imgCancion = "../images/NN.jpg";
        if (cancion.albumOfTrack.coverArt.sources.length != 0) {
            imgCancion = cancion.albumOfTrack.coverArt.sources[0].url;
        }

        //datos del album
        let urlAlbum = cancion.albumOfTrack.uri;
        let nameAlbum = cancion.albumOfTrack.name;

        //datos del artista
        let urlArtista = artista.uri;
        let nameArtista = artista.profile.name;
        let urlIArtista = artista.visuals.avatarImage.sources[0].url;
        if (artista.visuals.avatarImage.sources.length != 0) {
            urlIArtista = artista.visuals.avatarImage.sources[0].url;
        }

        contentHTML += `
        <div class="text-center col-12 col-md-6 m-auto" style="border-radius: 5px; background-color: #292929; heigth:100%">
            <img width="500" class="mx-auto img-fluid" src="${urlIArtista}" alt="">
        </div>
        <div class="col-10 col-md-6 my-auto p-2" style="background-color: #292929">
            <div style="width:50%; background-color: #EBEBEB; border-radius: 40px" class="mx-auto border border-success border-5">
                <img height="300" width="300" class="img-fluid mx-auto p-4" src="${imgCancion}" alt="" style="border-radius: 15%">
                <div class="text-center p-3">
                    <a href="${urlC}">
                        <p style="color: black;">${nameC} / </p>
                    </a>
                    <a href="${urlAlbum}">
                        <p style="color: black;">${nameAlbum}</p>
                    </a>
                </div>
                <div class=" pb-4 container row text-center justify-content-center align-items-center" style="width=270x">
                    <div class="col-1">
                        <i class="bi bi-play-fill"></i>
                    </div>
                    <div class="col-4">
                        <div class="progress ">
                            <div class="progress-bar bg-dark" role="progressbar" aria-label="Success example"
                                style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    }
    dato.innerHTML = contentHTML;
}

//carga el carrusel de artista en la pagina principal
async function cargarArtistasPaginaPrincipal() {
    let artistas = await buscarArtistaRandom(8);
    let container = document.getElementById('C-artistas');
    let contentHTML = '';
    let cont = 1;
    console.log(artistas)
    for (let v of artistas.artists.items) {
        var urlImg = "../images/NN.jpg";
        if (v.data.visuals.avatarImage != null) {
            urlImg = v.data.visuals.avatarImage.sources[0].url;
        }
        var name = v.data.profile.name;
        var urlA = v.data.uri;
        contentHTML += `
    <div class="col-12 col-md-4 col-lg-3">
        <div class="border border-1 card text-white bg-dark mb-3" style="width: 18rem; background: none;">
            <img width="500" height="300" style="border-radius: 80%" class="p-5 card-img-top" src="${urlImg}" alt="Card image cap">
            <div class="card-body">
                <a href="${urlA}"> <p style="color: white;" class="text-center  card-text">${name}</p></a>
            </div>
        </div>
    </div>`;
    }
    container.innerHTML = contentHTML;

}

//carga las cards de albumes en la pagina principal
async function cargarAlbumesPaginaPrincipal() {
    let albumes = await buscarAlbumsRandom(5);
    let container = document.getElementById('albumes');
    let contentHTML = '';
    let cont = 1;
    for (let v of albumes.albums.items) {
        let urlImg = v.data.coverArt.sources[0].url;
        let name = v.data.name;
        let urlA = v.data.uri;
        contentHTML += `
    <label class="item" for="t-${cont}">
        <img src="${urlImg}" alt="Imagen${name}">
        <a href="${urlA}"><h2>${name}</h2></a>
    </label>`;
        cont++;
    }
    container.innerHTML = contentHTML;
}

//busca un artista en especifico por el nombre (tipo=1) o el id
function buscarCancion(cancion, tipo) {
    let salida;
    if (tipo == 1) {
        salida = fetch('https://spotify23.p.rapidapi.com/search/?q=' + cancion + '&type=tracks&offset=0&limit=1&numberOfTopResults=5', credencial)
            .then(response => response.json());
    } else {
        salida = fetch('https://spotify23.p.rapidapi.com/search/?ids=' + cancion, credencial)
            .then(response => response.json());
    }
    return salida;
};

//busca un artista en especifico por el nombre
function buscarArtista(artista) {
    let salida = fetch('https://spotify23.p.rapidapi.com/search/?q=' + artista + '&type=artists&offset=0&limit=1&numberOfTopResults=5', credencial)
        .then(response => response.json());
    return salida;
};

//busca una determinada cantidad de albumes aleatorias
function buscarAlbumsRandom(cantidad) {
    let artistas = ['Anual', 'bunny', 'lil', 'niky', 'katty'];
    let salida = fetch('https://spotify23.p.rapidapi.com/search/?q=' + artistas[Math.floor(Math.random() * artistas.length)] + '&type=albums&offset=0&limit=' + cantidad + '&numberOfTopResults=' + cantidad, credencial)
        .then(response => response.json());
    return salida;
};

//busca una determinada cantidad de artistas aleatorias
function buscarArtistaRandom(cantidad) {
    let artistas = ['A', 'bu', 'l', 'ni', 'ka'];
    let salida = fetch('https://spotify23.p.rapidapi.com/search/?q=' + artistas[Math.floor(Math.random() * artistas.length)] + '3E&type=artits&offset=0&limit=' + cantidad + '&numberOfTopResults=' + cantidad, credencial)
        .then(response => response.json());
    return salida;
};

//busca una determinada cantidad de canciones aleatorias
function buscarCancionRandom(cantidad) {
    let artistas = ['A', 'bu', 'l', 'ni', 'ka'];
    let salida = fetch('https://spotify23.p.rapidapi.com/search/?q=' + artistas[Math.floor(Math.random() * artistas.length)] + '3E&type=tracks&offset=0&limit=' + cantidad + '&numberOfTopResults=' + cantidad, credencial)
        .then(response => response.json());
    return salida;
};