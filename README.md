# A World Of Music

A World Of Music es una Web Page dinámica de servicios digitales de información y sugerencias musicales que ofrece contenido multimedia sobre canciones, artistas y álbumes, desarrollada a partir de los datos extraídos de la API de Spotify. Este servicio está disponible en una diversa variedad de dispositivos, como lo son: Smartphone, portátiles, computadores de mesa o tabletas. Ten en una cuenta que A World Of Music es gratis, es decir, los usuarios pueden acceder a todo el contenido de manera gratuita, aunque con un límite de 500 búsquedas por mes.

### Home
***
Es la sección principal de la Web Page y consta de un banner con información básica de la página, además de un slider con sugerencias de álbumes y una sección con 8 artistas de distintos géneros que al usuario podrían interesarle.

> Es importante recalcar que las sugerencias 
> otorgadas por A World Of Music son aleatorias
> y se actualizan siempre que el usuario recargue la página web

La página web también permite al usuario realizar la búsqueda de una canción en específico por medio la caja de búsqueda ubicada en la parte superior derecha de la ventana. Se pueden presentar dos casos al realizar la búsqueda:
- **Canción encontrada**: La página muestra la información correspondiente a la canción (nombre y álbum al que pertenece), además de estar acompañada por la foto del artista respectivo.
- **Canción no encontrada**: La página despliega un mensaje de error y sugiere canciones variadas que el usuario puede escoger a falta de la buscada.

### Tecnologías
***
| Herramienta | Uso |
| ----------- | --- |
| Bootstrap | Implementación de complementos |
| HTML y CSS | Distribución y perspectiva del contenido |
| JavaScript | Funcionalidad |
| Git | Despliegue de la página |

### IDE
***
El entorno de desarrollo integrado que se utilizó para este proyecto fue Visual Studio Code.

### Instalación
***
Esta Web Page hace uso de la API de Spotify encontrada en la plataforma RapidAPI.

### Autores
***
Los autores de este proyecto son
- Lizeth Juliana Navarro Vargas - 1152024
- José Julián Álvarez Reyes - 1152025
- Julián Quiroz García - 1152029
